import { exec } from 'child_process';
import * as desktop_icon_info from 'desktop-icon-info';

export default function createWidget(widgetfolder: string) {
    console.log("Hi");
    // Main widget container
    var maincontainer = document.createElement("div");
    // Style
    maincontainer.className = "sessionwidgetcontainer"
    maincontainer.style.position = "absolute";
    maincontainer.style.top = "50%";
    maincontainer.style.left = "0";
    maincontainer.style.right = "0";
    maincontainer.style.transform = "translate(0, -50%)";
    maincontainer.style.textAlign = "center";
    // Button style
    var sessionwidgetstyle = document.createElement("style");
    sessionwidgetstyle.innerHTML = `
    .sessionwidgetcontainer img {
        width: 64px;
        height: 64px;
        display: inline-block;
        margin: 0 10px;
        cursor: pointer;
        transition: 0.2s;
    }
    .sessionwidgetcontainer svg:hover, .sessionwidgetcontainer img:hover {
        transform: scale(1.15);
    }
    `;
    maincontainer.appendChild(sessionwidgetstyle);

    // Returns an image object containing the icon.
    var geticon = function(iconname: string) {
        // Get the base64-encoded icon
        let appicon = document.createElement("img");
        appicon.src = "data:image/png;base64," + desktop_icon_info.GetIcon(iconname, 64);

        return appicon;
    }

    // Get the "system-shutdown"-icon
    let shutdownicon: HTMLElement = geticon("system-shutdown");
    shutdownicon.onclick = function() {
        exec("poweroff");
    }
    maincontainer.appendChild(shutdownicon);

    let rebooticon: HTMLElement = geticon("system-reboot");
    rebooticon.onclick = function() {
        exec("reboot");
    }
    maincontainer.appendChild(rebooticon);


    return maincontainer;
}